var indexSectionsWithContent =
{
  0: "_bcdeflmnoprstuvz",
  1: "bcfz",
  2: "dfmz",
  3: "_mz",
  4: "_bcdeflnoprstuv",
  5: "cz",
  6: "sz",
  7: "sz",
  8: "cmnz",
  9: "z"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Pages"
};

