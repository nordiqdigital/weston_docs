var indexSectionsWithContent =
{
  0: "dstz",
  1: "z",
  2: "z",
  3: "z",
  4: "dst",
  5: "z",
  6: "z"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "defines",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Macros",
  6: "Pages"
};

